const calculatingBodyMassIndex = (peso,altura) => peso/(altura**2);
const prompt = require('prompt')
const fn = async () =>{
    prompt.start();
    const res = await prompt.get(['weight','height']);
    let valueBodyMassaIndex = calculatingBodyMassIndex(res.weight,res.height);
    console.log(`${calculatingBodyMassIndex(res.weight,res.height).toFixed(2)}`);
    if (valueBodyMassaIndex<17){
        console.log('Muito abaixo do peso')
    }else if (valueBodyMassaIndex<18.5){
        console.log('Abaixo do peso')
    }else if(valueBodyMassaIndex<25){
        console.log('Peso normal')
    }else if(valueBodyMassaIndex<30){
        console.log('Sobrepeso')
    }else if(valueBodyMassaIndex<35){
        console.log('Obesidade grau I')
    }else if(valueBodyMassaIndex<40){
        console.log('Obesidade grau II')
    }else{
        console.log('Obesidade grau III')
    }
}

fn();