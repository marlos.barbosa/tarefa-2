const prompt = require('prompt')
const readArray = require(`./shiftingArray_2`)
const readShift = require(`./shiftingArray_3`)
const fn3 = async ()=>{
    prompt.start();
    const toBeShifted = await readArray(prompt);
    const shift = await readShift(prompt);
    toBeShifted.push.apply( toBeShifted, toBeShifted.splice( 0, shift ) );
    console.log(toBeShifted)
}
fn3()
