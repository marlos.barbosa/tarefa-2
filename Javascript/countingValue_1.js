const fn = async (prompt)=>{
    const interestNumber = await prompt.get(['toBeCounted']);
    return parseInt(interestNumber.toBeCounted,10)
}
module.exports = fn